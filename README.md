# Keycloak Snap Package

This is the Snapcraft package for Keycloak maintained by spicule.

To install the package you can run

    snap install keycloak-standalone-spicule


